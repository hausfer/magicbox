package edu.ecpi.MagicSquare;

public class Main2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
        if(args.length == 0){
            MagicSquare m1 = new MagicSquare();
            MagicSquare m2 = new MagicSquare();
            m1.setN(2);
            m2.setN(7);
		    System.out.println(m1 + "\n" + m2);
		}
        else{
        	MagicSquare m = new MagicSquare();
        	for(int i=0; i<args.length; i++){
        		m.setN(Integer.parseInt(args[i]));
        		System.out.println(m);
        	}
        }
	}
}
