package edu.ecpi.MagicSquare;

public class MagicSquare {
   protected int[][] square;
   protected int n;
   protected int currentRow;
   protected int currentCol;
   
   public void setN(int n){
	   this.n =n;
	   if (n%2==0){
		   System.out.println("Please enter odd number!");
		   return;
	   }
	   else{
		   computeSquare();
	   }
   }
   public int getN(){
	   return n;
   }
   public String toString(){
	   StringBuffer result = new StringBuffer();
	   for(int row = 0; row < n; row++){
		   for(int col = 0; col < n; col++){
			   result.append(String.format("%1$4s",square[row][col]));
		   }
		   result.append("\n");
	   }
	   return result.toString();
   }
   private int[][] initSquare(int size){
	   return new int[size][size];
   }
   private void computeSquare(){
	   square = initSquare(n);
	   currentRow = 0;
	   currentCol = n/2;
	   square[currentRow][currentCol] = 1;
	   int nSquare = n*n;
       for (int i=2; i<=nSquare; i++){
    	  
		   int nextRow = subOne(currentRow);
		   int nextCol = addOne(currentCol);
		   
		       if (square[nextRow][nextCol] == 0){
		    	   square[nextRow][nextCol] = i;
		       }   
		       else {
			       nextRow = addOne(currentRow);
				   nextCol = currentCol;
				   square[nextRow][nextCol] = i;
		       }

		   currentCol = nextCol;
		   currentRow = nextRow;
	   }
   }
   private int addOne(int value){
	   return (value+1)%n;
   }
   private int subOne(int value){
	   return (value+n-1)%n;
   }
}
   
